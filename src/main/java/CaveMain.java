import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CaveMain {

    public static void main(String[] args) {
        Cave cave = new Cave();

        Dragon dragon1 = new Dragon("Wildfire", 1000, Color.RED, 20);
        Dragon dragon2 = new Dragon("Aegon", 1202, Color.RED, 33);
        Dragon dragon3 = new Dragon("Blackrock", 707, Color.BLACK, 35);
        Dragon dragon4 = new Dragon("Maegor", 1100, Color.BLACK, 19);
        Dragon dragon5 = new Dragon("Viserys", 333, Color.GREEN, 14);
        Dragon dragon6 = new Dragon("Arryn", 606, Color.GREEN, 31);
        Dragon dragon7 = new Dragon("Targaryen", 1331, Color.GOLD, 37);
        Dragon dragon8 = new Dragon("Daenerys", 1031, Color.GOLD, 29);

        cave.addDragon(dragon1);
        cave.addDragon(dragon2);
        cave.addDragon(dragon3);
        cave.addDragon(dragon4);
        cave.addDragon(dragon5);
        cave.addDragon(dragon6);
        cave.addDragon(dragon7);
        cave.addDragon(dragon8);


        cave.print();

        System.out.println("\n");
        cave.printNames();

        System.out.println("\n");
        cave.printNamesAndColorSortedByName();

        System.out.println("\n");
        System.out.println("The oldest dragon have " + cave.theOldest() + " years");

        System.out.println("\n");
        System.out.println("Max wingspan is " + cave.maxWingSpan() + " meters.");

        System.out.println("\n");
        System.out.println(("The longest dragon name have " + cave.getMaxCharCount() + " chars."));

        System.out.println("\n");
        System.out.println(cave.dragonsWithThisColor(Color.RED));

        System.out.println("\n");
        System.out.println(cave.listOfAllNames());

        System.out.println("\n");
        System.out.println(cave.colorsOfDragonsCapital());

        System.out.println("\n");
        System.out.println(cave.allDragonSortedByNatural());

        System.out.println("\n");
        System.out.println(cave.allDragonSortedByAge());

        System.out.println("\n");
        System.out.println(cave.moreThanAllAge(1700));

//        System.out.println("\n");
//        System.out.println(cave.moreThanAllAge(1700));

        //dwie poniższe metody odwołują się do tej samej metody
        System.out.println("\n");
        System.out.println(cave.eggs());

        System.out.println("\n");
        cave.eggs().forEach(System.out::println);

    }

}
