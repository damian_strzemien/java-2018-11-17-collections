import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @author Damian Strzemien;
 * @version 1.0
 * @return Metody pozwalające dodać i usunąć smoka.
 */
public class Cave {
    private List<Dragon> dragons = new ArrayList<Dragon>();

    public void addDragon(Dragon dragon) {
        dragons.add(dragon);
    }

    public void rmDragon(Dragon dragon) {
        dragons.remove(dragon);
    }

    /**
     * @return print - return all dragons with all parameters
     */
    public void print() {
        dragons.forEach(System.out::println);
    }

    /**
     * @return printNames - return all dragons names
     */
    public void printNames() {
//        dragons.forEach(dragon -> System.out.println(dragon.getName()));
        dragons.stream().map(Dragon::getName).forEach(System.out::println);
    }

    /**
     * @return printNamesAndColorSortedByName - return dragons names sorted with alphabet and print dragons color
     */
    public void printNamesAndColorSortedByName() {
        List<Dragon> temp = dragons.stream()
                .sorted((d1, d2) -> d1.getName().compareTo(d2.getName())).collect(Collectors.toList());
        temp.forEach(dragon -> System.out.println("Name: " + dragon.getName() + ", color: " + dragon.getColor()));
    }

    /**
     * @return the Oldest - return one drogon of all, who is most years.
     */
    public int theOldest() {
        return dragons.stream().mapToInt(Dragon::getAge).max().orElse(0);
    }

    /**
     * @return maxWingSpan - return one dragon of all, who have biggest wingspan
     */
    public int maxWingSpan() {
        return dragons.stream().mapToInt(Dragon::getWingspan).max().orElse(0);
    }

    /**
     * @return getMaxCharCount - return numbers of characters with the longest name
     */
    public int getMaxCharCount() {
        return dragons.stream().map(Dragon::getName).mapToInt(String::length).max().orElse(0);

    }

    /**
     * @param color - return list of dragons with implements/given color
     */
    public List<Dragon> dragonsWithThisColor(Color color) {
        return dragons.stream().filter(dragon -> dragon.getColor().equals(color)).collect(Collectors.toList());
    }

    /**
     * @return listOfAllNames - return list of all dragons by names
     */
    public List<String> listOfAllNames() {
        return dragons.stream().map(Dragon::getName).collect(Collectors.toList());
    }

    /**
     * @return colorsOfDragonsCapital - return list of color dragons write capital letter
     */
    public List<Color> colorsOfDragonsCapital() {
        return dragons.stream().map(Dragon::getColor).collect(Collectors.toList());
    }

    /**
     * @return allDragonSortedByNatural - return list with dragon who was sorted by natural prefernce
     */
    public List<Dragon> allDragonSortedByNatural() {
        return dragons.stream().sorted().collect(Collectors.toList());
    }

    /**
     * @return allDragonSortedByAge - return list of dragons with sorted by they age
     */
    public List<Dragon> allDragonSortedByAge() {
        return dragons.stream().sorted((dragon, t1) -> dragon.getAge() - t1.getAge()).collect(Collectors.toList());
    }

    public boolean moreThanAllAge(int giveNumber) {
        int summing = dragons.stream().map(Dragon::getAge).collect(Collectors.summingInt(i -> i));
        if (summing > giveNumber) {
            return false;
        }
        return true;
    }

//    public boolean anyoneHaveColor(String giveColor) {
//    }

    public List<Egg> eggs() {
        return dragons.stream().map(Egg::new).collect(Collectors.toList());
    }

//    public List<Egg> eggsAndWingspan(int giveWingspan) {
//    }

}




