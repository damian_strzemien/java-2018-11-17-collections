
public class Dragon implements Comparable<Dragon> {
    /**
     * @author Damian Strzemień
     * @version 1.0
     *
     */

    private String name;
    private int age;
    private Color color;
    private int wingspan;

    public Dragon(String name, int age, Color color, int wingspan) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.wingspan = wingspan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getWingspan() {
        return wingspan;
    }

    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dragon dragon = (Dragon) o;

        if (age != dragon.age) return false;
        if (wingspan != dragon.wingspan) return false;
        if (name != null ? !name.equals(dragon.name) : dragon.name != null) return false;
        return color != null ? color.equals(dragon.color) : dragon.color == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + wingspan;
        return result;
    }

    @Override
    public String toString() {
        return "Dragon{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", color=" + color +
                ", wingspan=" + wingspan +
                '}';
    }

    @Override
    public int compareTo(Dragon dragon) {
        int ret = color.compareTo(dragon.color);
        if (ret == 0) {
            ret = age - dragon.age;
        }
        if (ret == 0) {
            ret = wingspan - dragon.wingspan;
        }
        if (ret == 0) {
            ret = name.compareTo(dragon.name);
        }
        return 0;
    }

}
