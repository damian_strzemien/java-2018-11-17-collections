public class Egg {

    private Color color;


    public Color getColor() {
        return color;
    }

    public Egg(Dragon dragon) {
        color = dragon.getColor();

    }

    @Override
    public String toString() {
        return "Egg{" +
                "color=" + color +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Egg egg = (Egg) o;

        return color == egg.color;
    }

    @Override
    public int hashCode() {
        return color != null ? color.hashCode() : 0;
    }

    public void setColor(Color color) {
        this.color = color;
    }


}
